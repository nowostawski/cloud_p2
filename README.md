### What is this repository for? ###

Assignment 2 in imt2681

### Database and update ###

I decided to feed database with last seven exchange rates before app went to production.
This solves a problem if Latest or Average request would be made before database would be updated.

A problem is still with a not-working Clock proccess, but whenever Webapp proccess is waken up of request, then Clock is also starting with update check.
This is still not-fully convienient because timing (update-request) and if there was no request in a few days, there will be a gap in database anyway.
Working Clock as a seperate app not a proccess could be a solution, but this is also limited by Heroku


### Verification issues ###

I received a little strange response from a verification system, regarding test coverage. Here is the first one (forgotten to run go fmt)

{"score":0.8333333,"tests":[{"desc":"TestRepo Clone","ok":true,"details":"Repo cloned."},{"desc":"TestRepo Build","ok":true,"details":"Repo builds OK."},{"desc":"TestRepo Tools","ok":false,"details":"[{gofmt: 0}, {go_vet: 1}, {golint: 1}, {gocyclo: 1}]"},{"desc":"TestRepo TestCoverage","ok":true,"details":"Test coverage: 0.6200"},{"desc":"TestAPI/mainroot /","ok":true,"details":"Bad Request"},{"desc":"TestAPI/partial /projectinfo/v1/","ok":true,"details":"Bad Request"}],"time":"28.40247027s"}

and here after I run it

{"score":1,"tests":[{"desc":"TestRepo Clone","ok":true,"details":"Repo cloned."},{"desc":"TestRepo Build","ok":true,"details":"Repo builds OK."},{"desc":"TestRepo Tools","ok":true,"details":"[{gofmt: 1}, {go_vet: 1}, {golint: 1}, {gocyclo: 1}]"},{"desc":"TestRepo TestCoverage","ok":true,"details":"Test coverage: 0.5330"},{"desc":"TestAPI/mainroot /","ok":true,"details":"Bad Request"},{"desc":"TestAPI/partial /projectinfo/v1/","ok":true,"details":"Bad Request"}],"time":"18.378534894s"}

I checked that there were acctually small format changes, but anyway test coverage went from 62 to 53 %
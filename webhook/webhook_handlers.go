package webhook

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"strings"
)

/*
HandleSubscribe Function that handle root/exchange/ POST requests
to subscribe a webhook
response: new webhook ID
*/
func HandleSubscribe(w http.ResponseWriter, r *http.Request) {

	var newWebhook Webhook
	errorMsg := "Cannot subscribe new webhook. Use POST with proper payload."

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			http.Error(w, errorMsg, 400)
		}
		json.Unmarshal(JSONdata, &newWebhook)
		newWebhook.ID = bson.NewObjectId().Hex()
		//newWebhook.ID = createUniqueId()
		insertWebhookIntoDb(newWebhook)
		fmt.Fprintf(w, "%s", newWebhook.ID)
	} else {
		http.Error(w, errorMsg, 400)
	}
}

/*
HandleWebhook Function that handle root/exchange/{id} GET and DELETE requests
response: view or delete
*/
func HandleWebhook(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/")

	if len(parts) == 3 || (len(parts) == 4 && len(parts[3]) == 0) { // root/exchange/{id}
		webhookID := parts[2]
		if len(webhookID) > 0 { // not root/exchange/nil
			webhookID = parts[2]

			switch r.Method {
			case "GET":
				webhook, err1 := getWebhookFromDatabase(webhookID)
				if err1 != nil {
					http.Error(w, "Couldn't find webhook "+webhook.ID+" in database.", 400)
				}
				finalObject := SubscribePayload{webhook.WebhookURL, webhook.BaseCurrency,
					webhook.TargetCurrency, webhook.MinTriggerValue,
					webhook.MaxTriggerValue}
				out, _ := json.Marshal(finalObject)
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprintf(w, "%s", out)

				//fmt.Fprintln(w, "Current webhook data:")	// webhook json ???
				//fmt.Fprintln(w, webhook)

				break
			case "DELETE":
				err2 := removeWebhookData(webhookID)
				if err2 == nil {
					fmt.Fprintln(w, "Successfully removed webhook "+webhookID)

				} else {
					http.Error(w, "Couldn't remove webhook "+webhookID+" from database.", 400)
				}
				break

			default:
				http.Error(w, "Wrong request method: "+r.Method+" (use GET to display info, or DELETE to remove webhook)", 400)
			}
		} else {
			// root/exchange/nil -> redirect to handleSubscribe??
		}
	} else {
		http.Error(w, "Couldn't find webhook. Use url format: /exchange/{id}", 400)
	}
}

/*
HandleLatestRate Function that handle root/exchange/latest
*/
func HandleLatestRate(w http.ResponseWriter, r *http.Request) {

	var latestRates LatestPayload
	//fmt.Fprintln(w, r.Method)

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err1 := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err1 != nil {
			http.Error(w, "Cannot get latest rates. Use POST with proper payload.", 400)
		}
		json.Unmarshal(JSONdata, &latestRates)

		currentRate, err2 := getLatestRate(latestRates.BaseCurrency, latestRates.TargetCurrency)
		if err2 == nil {
			fmt.Fprintln(w, currentRate)
		} else {
			http.Error(w, "Couldn't get current rate.", 400)
		}

	} else {
		//fmt.Println(r.Method)
		http.Error(w, "Cannot get latest rates. Use POST with proper payload.", 400)
	}
}

/*
HandleUpdateAllRateChanges Function that handle root/exchange/evaluationtrigger
*/
func HandleUpdateAllRateChanges(w http.ResponseWriter, r *http.Request) {
	successMsg, err := UpdateRates()
	if err != nil {
		http.Error(w, err.Error(), 400) // failures during update
	} else {
		fmt.Fprintln(w, successMsg) // "skipped" or "updated"
	}
}

/*
HandleAverageRate Function that handle root/exchange/average
*/
func HandleAverageRate(w http.ResponseWriter, r *http.Request) {

	var averageRates LatestPayload

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err1 := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err1 != nil {
			http.Error(w, "Cannot get average rates. Use POST with proper payload.", 400)
		}
		json.Unmarshal(JSONdata, &averageRates)

		recentSeven, err := getRecentSevenRatesFromDB()
		if err != nil {
			http.Error(w, "Couldn't get data from database", 400)
		} else if len(recentSeven) < 1 {
			http.Error(w, "Database empty. Run /evaluationtrigger to fetch data from Ticker", 400)
		} else {
			// non-empty array
			sum := float32(0)

			if strings.Compare(averageRates.BaseCurrency, "EUR") == 0 {
				// rate EUR:x
				for _, rate := range recentSeven {
					sum = sum + rate.RatesList[averageRates.TargetCurrency]
				}
				fmt.Fprint(w, sum/float32(len(recentSeven)))
			} else if strings.Compare(averageRates.TargetCurrency, "EUR") == 0 {
				// rate x:EUR
				for _, rate := range recentSeven {
					sum = sum + rate.RatesList[averageRates.BaseCurrency]
				}
				if sum > 0 {
					fmt.Fprint(w, float32(len(recentSeven))/sum)
				} else {
					http.Error(w, "No data to calculate", 400)
				}
			} else {
				// rate x:y
				sumBase := float32(0)
				sumTarget := float32(0)
				for _, rate := range recentSeven {
					sumBase = sumBase + rate.RatesList[averageRates.BaseCurrency]
					sumTarget = sumTarget + rate.RatesList[averageRates.TargetCurrency]
				}
				if sumBase > 0 {
					fmt.Fprint(w, sumTarget/sumBase)
				} else {
					http.Error(w, "No data to calculate", 400)
				}
			}
		}

	} else {
		//fmt.Println(r.Method)
		http.Error(w, "Cannot get average rates. Use POST with proper payload.", 400)
	}

}

func invokeWebhooks(currentRates Rates) (string, error) {
	errorFlag := false

	webhooks, err1 := getAllWebhooksFromDb()
	if err1 != nil {
		return "Couldn't get webhooks from db", err1
	}

	if len(webhooks) < 1 {
		return "No webhooks to invoke", nil
	}

	// at least one webhook to consider
	for _, hook := range webhooks {
		//current := currentRates.RatesList[hook.TargetCurrency]
		var current float32
		loopFlag := true
		minTr := hook.MinTriggerValue
		maxTr := hook.MaxTriggerValue

		if strings.Compare(hook.BaseCurrency, "EUR") == 0 {
			// if base = "EUR" ok, just take value from struct
			current = currentRates.RatesList[hook.TargetCurrency]
		} else if strings.Compare(hook.TargetCurrency, "EUR") == 0 {
			// need to recalculate triggers to another base currency with target "EUR"
			baseFactor := currentRates.RatesList[hook.BaseCurrency]
			if baseFactor > 0 { // base currency exists!
				current = float32(1) / baseFactor // [NOK:EUR] == 1/[EUR:NOK]
			} else {
				loopFlag = false
				errorFlag = true
			}

		} else { // need to recalculate triggers to another base and target currency
			baseFactor := currentRates.RatesList[hook.BaseCurrency]
			if baseFactor > 0 { // base currency exists!
				current = currentRates.RatesList[hook.TargetCurrency] / baseFactor
				// [PLN:NOK] = [EUR:NOK]/[EUR:PLN]
			} else {
				loopFlag = false
				errorFlag = true
			}
		}

		if loopFlag == true && (minTr > current || maxTr < current) {
			// publish change
			err2 := publishChange(hook, current)
			if err2 != nil {
				errorFlag = true
			}
		}
	}

	if errorFlag == true {
		return "", errors.New("Couldn't invoke all webhooks")
	}

	return "Webhooks invoked", nil
}

func publishChange(webhook Webhook, currentRate float32) error {

	url := webhook.WebhookURL

	var jsonStr []byte

	jsonStr, _ = json.Marshal(formatStandardInvokePayload(webhook, currentRate))

	// based on https://stackoverflow.com/questions/24455147/how-do-i-send-a-json-string-in-a-post-request-in-go
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return err
	}
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status) // log
	//fmt.Println("response Headers:", resp.Header)		// log
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body)) // log
	// end of code from stackoverflow

	if resp.StatusCode == http.StatusBadRequest { // case when all communication is ok
		return errors.New("Payload rejected") // but not correct payload format
	}

	return nil
}

func formatStandardInvokePayload(webhook Webhook, currentRate float32) InvokePayload {
	var payload InvokePayload
	payload.BaseCurrency = webhook.BaseCurrency
	payload.TargetCurrency = webhook.TargetCurrency
	payload.CurrentRate = currentRate
	payload.MinTriggerValue = webhook.MinTriggerValue
	payload.MaxTriggerValue = webhook.MaxTriggerValue

	return payload
}

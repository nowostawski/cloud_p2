package main

import (
	"bitbucket.org/TomaszRudowski/cloud_p2/webhook"
	"net/http"
	"os"
)

func main() {

	http.HandleFunc("/exchange/", webhook.HandleWebhook)  // /root/{id} GET, DELETE
	http.HandleFunc("/exchange", webhook.HandleSubscribe) // /root subscribe
	http.HandleFunc("/exchange/latest/", webhook.HandleLatestRate)
	http.HandleFunc("/exchange/latest", webhook.HandleLatestRate) // problems with method in postman??
	http.HandleFunc("/exchange/average/", webhook.HandleAverageRate)
	http.HandleFunc("/exchange/average", webhook.HandleAverageRate) // problems with method in postman??
	http.HandleFunc("/exchange/evaluationtrigger/", webhook.HandleUpdateAllRateChanges)
	//	http.HandleFunc("/", handleWrongURL)				// without /exchange
	port := os.Getenv("PORT")
	http.ListenAndServe(":"+port, nil)

}

// https://TomaszRudowski@bitbucket.org/TomaszRudowski/cloud_p2.git
